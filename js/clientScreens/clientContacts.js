$(function () {
  $("#clientContactScreen").on("click", "#addNewContact", function () {
    $("#panelContainer").load("/routes/clientScreens/contacts/addEditContacts.html", function () {
      $(".panelScreen").css("display", "flex");
    });
  });

  $("#clientContactScreen").on("click", "#addNewAddress", function () {
    $("#panelContainer").load("/routes/clientScreens/contacts/addEditAddress.html", function () {
      $(".panelScreen").css("display", "flex");
    });
  });

});

