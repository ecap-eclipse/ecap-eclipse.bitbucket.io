// html elements
const submitFinBtn      = document.getElementById('submit');
const setUpProgress     = document.getElementById('setUpProgres');

  $("#newClientSetup").on("click", "#submit", submitToFin);

  function submitToFin(e){
      console.log('submitted to finance')
      e.preventDefault();
      $(".finVerify").removeClass("hide");
      $('.verified').width('260');
      $('.verified').addClass('align-left');

      $("input[type='checkbox']").prop('checked', false);
      $('#submit').attr('disabled', 'disabled');

      $('#submit').attr('value', 'Submit to Onboarding');
      setUpProgress.textContent = 'Finance Verified';
  }

//disable button until input fields are filled
(function() {
  $("input[type='checkbox']").on('keyup click', function() {

      let filled = false;
      $("input[type='checkbox']").each(function() {
          if ($(this).is(':checked')) {
              filled = true;
          }
          else{
              filled = false;
          }
      });

      if (filled) {
        $('#submit').removeAttr('disabled');
      } else {
        $('#submit').attr('disabled', 'disabled');
      }
  });
})()