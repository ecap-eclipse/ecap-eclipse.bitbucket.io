$(function () {
  $("#clientSettingsScreen").on("click", "#addNewBanking", function () {
    $("#panelContainer").load("/routes/clientScreens/clientSettings/addEditBank.html", function () {
      $(".panelScreen").css("display", "flex");
    });
  });

  $("#clientSettingsScreen").on("click", "#addNewCompliance", function () {
    $("#panelContainer").load("/routes/clientScreens/clientSettings/addEditCompliance.html", function () {
      $(".panelScreen").css("display", "flex");
    });
  });

  
});
  