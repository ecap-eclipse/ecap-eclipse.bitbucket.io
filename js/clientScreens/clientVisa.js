$(function () {
  $("#clientVisaPage").on("click", "#openVisaDetails", function () {
    $("#panelContainer").load("/routes/clientScreens/visa/VisaDetail.html", function () {
      $(".panelScreen").css("display", "flex");
    });
  });
});