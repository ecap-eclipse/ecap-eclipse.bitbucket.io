$(function () {
  var mouseInsidePanel = false;
  $("#panelContainer").on("click", "#save", function() {
    $(".panelScreen").css("display", "none");
  });

  $("#panelContainer").on("click", "#cancel", function() {
    $(".panelScreen").css("display", "none");
  });

  // Determines if mouse is on panel or not
  $("#panelContainer").on("mouseenter", function () {
    mouseInsidePanel = true;
  });
  $("#panelContainer").on("mouseleave", function () {
    mouseInsidePanel = false;
  });
  // If  mouse is off panel on mouseDown panel will close
  $("body").on("mousedown", function (e) {
    if (!mouseInsidePanel && $(".panelScreen").is(":visible")) {
    $(".panelScreen").css("display", "none");
    }
  });
});