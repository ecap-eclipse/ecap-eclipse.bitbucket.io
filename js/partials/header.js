$(function () {

//hides or shows the placeholder text based of input value at keypress
  $("#header").on("keydown", ".headerSearch", function () {
    $(".headerSearchPlaceholder").addClass("visibility");
  });
  $("#header").on("keyup", ".headerSearch", function () {
    if (!$(".headerSearch").val()) {
      $(".headerSearchPlaceholder").removeClass("visibility");
    }
  });

  $("#header").on("click", "#sideBurgerLeft, #sideBurgerRight" ,function () {
    console.log("click");
    $(".logo, .logo2").toggleClass("display");
    $("#sideBurgerLeft, #sideBurgerRight").toggleClass("display");
    $("#main").toggleClass("collapsed");
    //$("#sideBar").toggleClass("collapsed");
  });

  $("#header").on("click", "#openPreferences" ,function () {
    $("#panelContainer").load("/routes/partials/userPreferences.html", function () {
      $(".panelScreen").css("display", "flex");
    });
  });
  
});