//Client Nav
$(function () {
    $("#main").on("click", "#clientAccounts", function () {
      $("#main").load("/routes/clientScreens/clientAccounts.html");
    });

    $("#main").on("click", "#clientSummary", function () {
      $("#main").load("/routes/clientScreens/clientSummary.html");
    });

    $("#main").on("click", "#clientContacts", function () {
      $("#main").load("/routes/clientScreens/contacts/clientContacts.html");
    });

    $("#main").on("click", "#clientFuel", function () {
        $("#main").load("/routes/clientScreens/fuel/clientFuel.html");
      });

    $("#main").on("click", "#clientVisa", function () {
      $("#main").load("/routes/clientScreens/visa/clientVisa.html");
    });

    $("#main").on("click", "#clientVendors", function () {
      $("#main").load("/routes/clientScreens/clientVendors.html");
    });

    $("#main").on("click", "#clientSettings", function () {
      $("#main").load("/routes/clientScreens/clientSettings/clientSettings.html");
    });
  });