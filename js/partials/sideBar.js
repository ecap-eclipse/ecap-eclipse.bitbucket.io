$(function () {
  $("body").on("click", "#sidebarCollapse", function () {
    $(".sideBarContainer > li").width("64px");
    $(".expandedContainer").hide();
    $("#sidebarCollapse").hide();
    $("#sidebarExpand").show();
    $('.header').addClass('collapsed');
    $('#main').addClass('expanded');
  });
  $("body").on("click", "#sidebarExpand", function () {
    $(".sideBarContainer > li").width("240px");
    $(".expandedContainer").show();
    $("#sidebarCollapse").show();
    $("#sidebarExpand").hide();
    $('#main').removeClass('expanded');
  });

  $("body").on("click", "#sideBurgerLeft", function () {
    $("#sideBar").hide();
    $('#main').addClass('sidebarClosed');
    $('.header').addClass('sidebarClosed');
    
  });

  $("body").on("click", "#sideBurgerRight", function () {
    $("#sideBar").show();
    $('#main').removeClass('sidebarClosed');
    $('.header').removeClass('sidebarClosed');
  });


  //Sidebar Nav
  $("#sideBar").on("click", "#clients", function () {
    $("#main").load("/routes/clientScreens/newClientSetup.html");
  });

  $("#sideBar").on("click", "#workSpace", function () {
    $("#main").load("/routes/clientScreens/clientAccounts.html");
  });

});