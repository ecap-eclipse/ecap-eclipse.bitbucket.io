

let errorMessage = 'Validation error';

  $("body").on("focusout", "#website", validateURL);
  $("body").on("focusout", ".emailInput", validateEmail);
  $("body").on("focusout", ".telInput", validatePhone);
  

  $('body').on('click', '.websiteLink', function(e){
    e.preventDefault;
  });

  // validate Website
  function validateURL(){
    const enteredURL= $(this).val();
    console.log(enteredURL);
  
    function isValidUrl(enteredURL) {
      const matchpattern = /^(?:(http|https):\/\/)?(?:[\w-]+\.)+[a-z]{2,6}$/gm;
      return matchpattern.test(enteredURL);
      
    }
    if (isValidUrl(enteredURL) == true){
      console.log("URL is Valid")
      $('.websiteIcon').addClass('active');
      $('input#website').removeClass('error');
      $('.errorMsg').hide();
      enteredURL.replace('https://', '');
      enteredURL.replace('http://', '');
      $('.websiteLink').attr('href', `https://${enteredURL}`);
    }
    if (isValidUrl(enteredURL) == false){
      console.log("URL is Invalid")
      $('.websiteIcon').removeClass('active');
      $('input#website').addClass('error');
      $('body').on('click', 'a.websiteLink', function(e){
        e.preventDefault;
      });
      
      errorMessage = 'Please enter a valid URL';
      
      if (!$(this).hasClass('errHandler')){
        $(this).after(`<span class="errorMsg">${errorMessage}<span>`);
        $(this).addClass('errHandler');
      }
    }
  }

  // validate Email Address
  function validateEmail(){
    const enteredEmail= $(this).val();
    console.log(enteredEmail);

    function isValidUrl(enteredEmail) {
      const matchpattern = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
      return matchpattern.test(enteredEmail);
    }
    if (isValidUrl(enteredEmail) == true){
      console.log("Email is Valid")
      $('input.emailInput').removeClass('error');
      $('.errorMsg').hide();
    }
    if (isValidUrl(enteredEmail) == false){
      console.log("Email is Invalid")
      $('input.emailInput').addClass('error');
      
      errorMessage = 'Please enter a valid email address';
      
      if (!$(this).hasClass('errHandler')){
        $(this).after(`<span class="errorMsg">${errorMessage}<span>`);
        $(this).addClass('errHandler');
      }
    }
  }

    // validate Phone Number
    function validatePhone(){
      const enteredPhone= $(this).val();
      console.log(enteredPhone);
  
      function isValidUrl(enteredPhone) {
        const matchpattern =   /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/g;
        return matchpattern.test(enteredPhone);
      }
      if (isValidUrl(enteredPhone) == true){
        console.log("Phone Number is Valid")
        $('input.telInput').removeClass('error');
        $('.errorMsg').hide();
      }
      if (isValidUrl(enteredPhone) == false){
        console.log("Phone Number is Invalid")
        $('input.telInput').addClass('error');
        
        errorMessage = 'Please enter a valid phone number';
        
        if (!$(this).hasClass('errHandler')){
          $(this).after(`<span class="errorMsg">${errorMessage}<span>`);
          $(this).addClass('errHandler');
        }
      }
    }


// error message  
  function errorMsg(){
    if (!$('.error').hasClass('errHandler')){
      $(this).after(`<span class="errorMsg">${errorMessage}<span>`);
      $(this).addClass('errHandler');
    }
  }